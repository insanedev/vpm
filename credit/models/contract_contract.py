# -*- coding:utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import timedelta, datetime
import logging, sys

_logger = logging.getLogger(__name__)


class ContractContract(models.Model):
    _inherit = 'contract.contract'


    credit_schemes_line_ids = fields.Many2many(
        'credit.credit_schemes',
        string='Esquemas de Crédito',
    )


    @api.multi
    def set_unit_price_invoiced_period(self):
        _partners = self.env['res.partner'].search([])
        for _partner in _partners:
            _contracts = self.env['contract.contract'].search([('partner_id','=',_partner.id),('active','=',True)])
            if(_contracts):
                for _contract in _contracts:
                    for _contract_lines in _contract.contract_line_ids:
                        start_date =  datetime.strptime(str(_contract_lines.next_period_date_start), "%Y-%m-%d") + timedelta(hours=5, minutes=00, seconds=1)
                        end_date =  datetime.strptime(str(_contract_lines.next_period_date_end), "%Y-%m-%d") + timedelta(days=1, hours=4, minutes=59, seconds=59)
                        orders = self.env['pos.order'].search([('company_id','=',_partner.company_id.id),('state_order_fac','=','n'),('order_type','=','Cŕedito'),('is_postpaid','=',True),('date_order','>=',start_date),('date_order','<=',end_date)])
                        importes_por_persona = dict()
                        res = []
                        if orders:
                            for o in orders:
                                if o.partner_id in importes_por_persona:
                                    importes_por_persona[o.partner_id] += o.amount_total
                                else:
                                    importes_por_persona[o.partner_id] = o.amount_total
                            for key, value in importes_por_persona.items():
                                res.append({
                                            'client_number': key.client_number,
                                            'cliente_principal': key.parent_id.name,
                                            'cliente': key.name,
                                            'importe': value,
                                            })
                            if(len(res)>0):
                                total_amount = 0.0
                                for itm in res:
                                    total_amount += float(itm['importe'])
                                    #_contract_line = self['contract.line'].browse(_contract_lines.id)
                                    _contract_lines.sudo().update({'price_unit':total_amount})
                                
                                _logger.warning(total_amount)

    @api.multi
    def set_unit_price_invoiced_period_account(self):
        _partners = self.env['res.partner'].search([])
        for _partner in _partners:
            _contracts = self.env['contract.contract'].search([('partner_id','=',_partner.id),('active','=',True)])
            if(_contracts):
                for _contract in _contracts:
                    for _contract_lines in _contract.contract_line_ids:
                        start_date =  datetime.strptime(str(_contract_lines.next_period_date_start), "%Y-%m-%d") + timedelta(hours=5, minutes=00, seconds=1)
                        end_date =  datetime.strptime(str(_contract_lines.next_period_date_end), "%Y-%m-%d") + timedelta(days=1, hours=4, minutes=59, seconds=59)                        

                        orders = self.env['pos.order'].search([('company_id','=',_partner.company_id.id),('state_order_fac','=','n'),('order_type','=','Cŕedito'),('is_postpaid','=',True),('date_order','>=',start_date),('date_order','<=',end_date)])
                        importes_por_persona = dict()
                        res = []

                        if orders:
                            for o in orders:
                                if o.partner_id in importes_por_persona:
                                    importes_por_persona[o.partner_id] += o.amount_total
                                else:
                                    importes_por_persona[o.partner_id] = o.amount_total
                            for key, value in importes_por_persona.items():
                                res.append({
                                            'client_number': key.client_number,
                                            'cliente_principal': key.parent_id.name,
                                            'cliente': key.name,
                                            'importe': value,
                                            })
                            if(len(res)>0):
                                total_amount = 0.0
                                for itm in res:
                                    total_amount += float(itm['importe'])
                                    #_contract_line = self['contract.line'].browse(_contract_lines.id)
                                    _contract_lines.sudo().update({'price_unit':total_amount})
                                
                                _logger.warning(total_amount)
                        
                        _invoices = self.env['account.invoice'].search([('partner_id','=',_partner.id),('date_invoice','>=',_contract_lines.next_period_date_start),('date_invoice','<=',_contract_lines.next_period_date_end)])
                        for _invoice in _invoices:
                            if(_invoice.invoice_line_ids):
                                for _invoice_line in _invoice.invoice_line_ids:
                                    if(_contract_lines.product_id.id == _invoice_line.product_id.id):
                                        if(total_amount>0):
                                            _invoice_line.sudo().update({'price_unit':total_amount})



class ResPartner(models.Model):
    _inherit = 'res.partner'


    credit_s_id = fields.Many2one(
        'credit.credit_schemes',
        string='Esquema de Crédito',
    )

    ids_schemes_contracts = fields.Many2many(
        'credit.credit_schemes',
        compute='_compute_schemes_credit',
        store=True,
        string='Esquemas de Crédito',
    )


    schemes_sub_id = fields.Many2one(
        'contract.scheme.contract',
        string='Esquema de Subsidio',
    )
    ids_schemes_sub = fields.Many2many(
        'contract.scheme.contract',
        compute='_compute_schemes_subsidio',
        store=False,
        string='Esquemas de Subsidio',
    )


    type_contract_hide = fields.Char(
        compute='_compute_type_credit',
        string='Tipo de contrato',
        help=' tipos de  contrato - , credito, prepago, mealplan, subsidio', 
    )

    credit_limit_computed = fields.Float(
        compute='_credit_limit_computed',
        string='Límite de Crédito',
        help=' campo para mostrar el limite de credito ', 
    )
    client_number = fields.Char(
        string='Número de Cliente',
    )
    saldo_credit = fields.Float(
        compute='_get_sales_saldo_partner',
        string='Saldo',
    )


    @api.multi
    @api.depends('credit_limit')
    def _credit_limit_computed(self):
        for rec in self:
            rec.credit_limit_computed = rec.credit_limit


    @api.multi
    @api.depends('contract_ids')
    def _compute_schemes_credit(self):
        
        try:
            
            acumulador = []
            partner_id = self.id
            contracts = self.env['contract.contract'].search([('partner_id','=',partner_id),('active','=',True)])

            for con in contracts:
                if con.type_contract == "credito":
                    for sch in con.credit_schemes_line_ids:
                        acumulador.append(sch.id)
            self.ids_schemes_contracts = [(6, 0, acumulador)]

            if(len(acumulador)==0):
                if(self.parent_id):
                    parent_partner = self.env['res.partner'].browse(self.parent_id.id)
                    if  parent_partner.type_contract_hide == "credito":
                        acumulador = []
                        for sch in parent_partner.ids_schemes_contracts:
                            acumulador.append(sch.id)
                        self.ids_schemes_contracts = [(6, 0, acumulador)]
                        #acumulador = []
                        #for contract in parent_partner.contract_ids:
                        #    acumulador.append(contract.id)
                        #self.contract_ids = [(6, 0, acumulador)]
        except Exception as e:
           exc_traceback = sys.exc_info() 
           #raise Warning(getattr(e, 'message', repr(e))+" ON LINE "+format(sys.exc_info()[-1].tb_lineno))

    @api.multi
    @api.depends('contract_ids')
    def _compute_schemes_subsidio(self):
        try:
            acumulador = []
            partner_id = self.id
            contracts = self.env['contract.contract'].search([('partner_id','=',partner_id),('active','=',True)])
            for con in contracts:
                if con.type_contract == "subsidio":
                    for sch in con.esquema_subsidio_ids:
                        acumulador.append(sch.id)
            self.ids_schemes_sub = [(6, 0, acumulador)]

            if(len(acumulador)==0):
                if(self.parent_id):
                    parent_partner = self.env['res.partner'].browse(self.parent_id.id)
                    if  parent_partner.type_contract_hide == "subsidio":
                        for sch in parent_partner.ids_schemes_sub:
                            acumulador.append(sch.id)
                        self.ids_schemes_sub = [(6, 0, acumulador)]
                        #acumulador = []
                        #for contract in parent_partner.contract_ids:
                        #    acumulador.append(contract.id)
                        #self.contract_ids = [(6, 0, acumulador)]
        except:
            pass



    @api.multi
    @api.depends('contract_ids')
    def _compute_type_credit(self):
        for rec in self:
            tipo = ""
            if rec.parent_id:
                partner_id = rec.parent_id.id
                contracts = self.env['contract.contract'].search([('partner_id','=',partner_id),('active','=',True)])
                for con in contracts:
                    if con.type_contract == "credito":
                        tipo = "credito"
                    elif con.type_contract == "prepago":
                        tipo = "prepago"
                    elif con.type_contract == "mealplan":
                        tipo = "mealplan"
                    elif con.type_contract == "subsidio":
                        tipo = "subsidio"
                rec.type_contract_hide = tipo
            else:
                partner_id = rec.id
                contracts = self.env['contract.contract'].search([('partner_id','=',partner_id),('active','=',True)])
                for con in contracts:
                    if con.type_contract == "credito":
                        tipo = "credito"
                    elif con.type_contract == "prepago":
                        tipo = "prepago"
                    elif con.type_contract == "mealplan":
                        tipo = "mealplan"
                    elif con.type_contract == "subsidio":
                        tipo = "subsidio"
                rec.type_contract_hide = tipo


    @api.multi
    @api.onchange('credit_s_id')
    def onchange_credit_s_id(self):
        #raise Warning(self._origin.read())
        current_credit_limit = self.credit_limit
        new_credit_limit = float(0)

        #if(float(current_credit_limit) > 0):
        #    new_credit_limit = float(current_credit_limit) + float(self.credit_s_id.quantity)
        #else:
        #    new_credit_limit = self.credit_s_id.quantity
        #self.update({"credit_limit":new_credit_limit})
#
        self.update({"credit_limit":self.credit_s_id.quantity})

        for partner_child in self.child_ids:
            _partner_child = self.env['res.partner'].browse(partner_child.id)
            if(_partner_child.credit_s_id):
                pass
            else:
                _partner_child.sudo().update({"credit_limit":0})
    
    @api.onchange('schemes_sub_id')
    def onchange_schemes_sub_id(self):
        current_credit_limit = self.credit_limit
        new_credit_limit = float(0)

        #if(float(current_credit_limit) > 0):
        #    new_credit_limit = float(current_credit_limit) + float(self.schemes_sub_id.qty)
        #else:
        #    new_credit_limit = self.schemes_sub_id.qty
        #self.update({"credit_limit":new_credit_limit})
        #self.update({"credit_limit":new_credit_limit})
        self.update({"credit_limit":self.schemes_sub_id.qty})
        
        for partner_child in self.child_ids:
            _partner_child = self.env['res.partner'].browse(partner_child.id)
            if(_partner_child.schemes_sub_id):
                pass
            else:
                _partner_child.sudo().update({"credit_limit":0})

    def write(self, vals):
        partner = super(ResPartner, self).write(vals)
        for partner in self:
            if(partner.child_ids):
                for partner_child in partner.child_ids:
                    _partner_child = self.env['res.partner'].browse(partner_child.id)
                    if(_partner_child.schemes_sub_id):
                        pass
                    else:
                        _partner_child.sudo().update({"credit_limit":0})
        return partner


    @api.multi
    def _get_sales_saldo_partner(self):
        for rec in self:
            suma = 0
            orders = self.env['pos.order'].search([('partner_id','=',rec.id),('state_order_fac','=','n'),('order_type','=','Cŕedito'),('is_postpaid','=',True)])
            for o in orders:
                suma += o.amount_total
            saldo = rec.credit_limit - suma
            rec.saldo_credit = saldo
