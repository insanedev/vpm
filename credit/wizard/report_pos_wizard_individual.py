# -*- coding:utf-8 -*-
from odoo import models, fields, api
from odoo import tools
from odoo.exceptions import ValidationError
from dateutil import relativedelta
from datetime import datetime, timedelta
import locale, base64

import logging


_logger = logging.getLogger(__name__)


class ReportPosIndividualWizard(models.TransientModel):
    _name = 'credit.report_pos_individual_wizard'
    _description = 'Wizard para el reporte de las ventas en POS Individual '


    start_date = fields.Datetime(computed='_default_date_report', string='Fecha y Hora inicial',)
    end_date  = fields.Datetime(computed='_default_date_report', string='Fecha y Hora Final',)
    partner_id = fields.Many2one('res.partner',string='Cliente',)
    check_mail = fields.Boolean(string='Enviar por Correo',)



    @api.multi
    @api.onchange('partner_id')
    def _default_date_report(self):
        localization = locale.getdefaultlocale()
        _logger.warning(localization)
        contracts = self.env['contract.contract'].search([('partner_id','=',self.partner_id.parent_id.id),('active','=',True)])
        for c in contracts:
            for lc in c.contract_line_ids:
                self.start_date =  datetime.strptime(str(lc.next_period_date_start), "%Y-%m-%d") + timedelta(hours=5, minutes=00, seconds=1)
                self.end_date =  datetime.strptime(str(lc.next_period_date_end), "%Y-%m-%d") + timedelta(days=1, hours=4, minutes=59, seconds=59)
        


    @api.multi
    def consult_report_individual_details(self):        
        one = self.start_date
        two = self.end_date
        start = one.strftime("%m-%d-%Y %H:%M:%S.%f")
        end = two.strftime("%m-%d-%Y %H:%M:%S.%f")
        res= []
        orders = self.env['pos.order.line'].search([('order_id.partner_id.id','=',self.partner_id.id),('order_id.state_order_fac','=','n'),('order_id.order_type','=','Cŕedito'),('order_id.is_postpaid','=',True),('order_id.date_order','>=',start),('order_id.date_order','<=',end)])
        for o in orders:
            res.append({
                'orden':o.order_id.name,
                'fecha': o.create_date,
                'producto': o.product_id.name,
                'importe': o.price_subtotal_incl,
                })
        return res

    @api.multi
    def get_report_individual_details(self):
        data = {
            'orders': self.consult_report_individual_details(),
            'start_date': self.start_date,
            'end_date': self.end_date,
            'client': self.partner_id.name,
            'client_number':self.partner_id.client_number,
            }
        report_data = self.env.ref('credit.action_report_credit_summary_individual').report_action(self, data=data,config=False)
        report, format = self.env.ref('credit.action_report_credit_summary_individual').render_qweb_pdf(data=data)
        
        if self.check_mail:
            self.send_mail_report(report, report_data)

        return report_data

    @api.multi
    def send_mail_report(self, report, report_data):
        _logger.warning("REPORT")
        _logger.warning(report_data['name'])

        template = self.env['mail.template'].search([('name','=','email.pos.report.individual')],limit=1)
        data = template.generate_email(self.id)

        attachment_data =  {
                                'name': "Emails :- " + str(report_data['name']),
                                'datas_fname': str(report_data['name']) + ".pdf",
                                'db_datas': base64.b64encode(report),
                            }

        data['email_to'] = self.partner_id.email
        server_ids = self.env['ir.mail_server'].search([])
        if server_ids:
            data['email_from'] = server_ids[0].smtp_user or False


        att_id = self.env['ir.attachment'].sudo().create(attachment_data)

        data['attachment_ids'] = [(6, 0, [att_id.id])]
        message_id = self.env['mail.mail'].sudo().create(data)
        #raise Warning(message_id)
        if message_id:
            message_id.sudo().send()
            template.sudo().send_mail(self.id, force_send=True)


    def send(self, template):
        # objeto odoo de correo
        mail = self.env['mail.mail']
        mail_data={
            'subject':'Reporte De Crédito Individual',
            'body_html': template['body_html'],
            'email_to': 'francisco-castillo-moo@hotmail.com',
            'email_from': template['email_from'],
        }
        mail_out=mail.create(mail_data)
        if mail_out:
            mail.send(mail_out)
            _logger.info("Reporte de Pos Enviado📬")