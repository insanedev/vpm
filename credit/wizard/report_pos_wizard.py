# -*- coding:utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
from dateutil import relativedelta
from datetime import datetime, timedelta
from odoo.http import request
import base64
import logging


_logger = logging.getLogger(__name__)


class ReportPosWizard(models.TransientModel):
    _name = 'credit.report_pos_wizard'
    _description = 'Wizard para el reporte de las ventas en POS '


    start_date = fields.Datetime(computed='_default_date_report', string='Fecha y Hora inicial')
    end_date  = fields.Datetime(computed='_default_date_report', string='Fecha y Hora Final', default=lambda self: fields.datetime.now())
    company_id = fields.Many2one('res.company',string='Compañia',)
    partner_id = fields.Many2one('res.partner',string='Cliente',)
    check_mail = fields.Boolean(string='Enviar por Correo')
    report_date_times_defaults = fields.Selection([('manual','Manual'),('daily','Diario'),('invoiced_period','Periodo facturable')], string='Reporte', default='invoiced_period')

    @api.multi
    @api.onchange('partner_id')
    def _default_date_report(self):
        contracts = self.env['contract.contract'].search([('partner_id','=',self.partner_id.id),('active','=',True)])
        for c in contracts:
            for lc in c.contract_line_ids:
                self.start_date =  datetime.strptime(str(lc.next_period_date_start), "%Y-%m-%d") + timedelta(hours=5, minutes=00, seconds=1)
                self.end_date =  datetime.strptime(str(lc.next_period_date_end), "%Y-%m-%d") + timedelta(days=1, hours=4, minutes=59, seconds=59)

    @api.multi
    @api.onchange('report_date_times_defaults')
    def _default_report_date_times_defaults(self):
        if(self.report_date_times_defaults=="invoiced_period"):
            contracts = self.env['contract.contract'].search([('partner_id','=',self.partner_id.id),('active','=',True)])
            for c in contracts:
                for lc in c.contract_line_ids:
                    self.start_date =  datetime.strptime(str(lc.next_period_date_start), "%Y-%m-%d") + timedelta(hours=5, minutes=00, seconds=1)
                    self.end_date =  datetime.strptime(str(lc.next_period_date_end), "%Y-%m-%d") + timedelta(days=1, hours=4, minutes=59, seconds=59)
        elif(self.report_date_times_defaults=="daily"):
            _company = self.env['res.company'].browse(self.env.user.company_id.id)
            self.start_date = _company.start_day_datetime
            self.end_date = _company.ends_day_datetime
        else:
            self.start_date = None
            self.end_date = None


    @api.multi
    def consult_credit_details(self):
        one = self.start_date
        two = self.end_date
        start = one.strftime("%m-%d-%Y %H:%M:%S.%f")
        end = two.strftime("%m-%d-%Y %H:%M:%S.%f")
        orders = self.env['pos.order'].search([('company_id','=',self.company_id.id),('state_order_fac','=','n'),('order_type','=','Cŕedito'),('is_postpaid','=',True),('date_order','>=',start),('date_order','<=',end)])
        importes_por_persona = dict()
        res = []
        if orders:
            for o in orders:
                if o.partner_id in importes_por_persona:
                    importes_por_persona[o.partner_id] += o.amount_total
                else:
                    importes_por_persona[o.partner_id] = o.amount_total
            for key, value in importes_por_persona.items():
                res.append({
                    'client_number': key.client_number,
                    'cliente_principal': key.parent_id.name,
                    'cliente': key.name,
                    'importe': value,
                    })
            return res
        else:
            raise ValidationError("No Tiene Información para Mostrar ")

    @api.multi
    def get_report_credit_details(self):
        
        data = {
                    'orders': self.consult_credit_details(),
                    'start_date': self.start_date,
                    'end_date': self.end_date,
                }

        report_data = self.env.ref('credit.action_report_credit_summary').report_action(self, data=data,config=False)
        report, format = self.env.ref('credit.action_report_credit_summary').render_qweb_pdf(data=data)

        if self.check_mail:
            self.send_mail_report(report, report_data)
        return report_data

    @api.multi
    def sale_report_pos(self):
        one = self.start_date
        two = self.end_date
        start = one.strftime("%m-%d-%Y %H:%M:%S.%f")
        end = two.strftime("%m-%d-%Y %H:%M:%S.%f")
        view_id = self.env.ref('point_of_sale.view_pos_order_tree').id
        domain = ['&',('date_order','>=',start),('date_order','<=',end)]
        return {
            'type': 'ir.actions.act_window',
            'name': 'Ventas de los Clientes',
            'res_model': 'pos.order',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(view_id, 'tree')],
            'domain': domain,
        }

    @api.multi
    def send_mail_report(self, report, report_data):
        

        template = self.env['mail.template'].search([('name','=','email.pos.report.general')],limit=1)
        data = template.generate_email(self.id)

        attachment_data =  {
                                'name': "Emails :- " + str(report_data['name']),
                                'datas_fname': str(report_data['name']) + ".pdf",
                                'db_datas': base64.b64encode(report),
                            }

        data['email_to'] = "rockscripts@gmail.com"#self.partner_id.email
        server_ids = self.env['ir.mail_server'].search([])
        if server_ids:
            data['email_from'] = server_ids[0].smtp_user or False


        att_id = self.env['ir.attachment'].sudo().create(attachment_data)

        data['attachment_ids'] = [(6, 0, [att_id.id])]
        message_id = self.env['mail.mail'].sudo().create(data)
        _logger.warning("MAILID")
        _logger.warning(message_id)
        if message_id:
            message_id.sudo().send()
            mail_id = template.sudo().send_mail(self.id, force_send=True)
            _logger.warning("MAILID")
            _logger.warning(mail_id)


    def send(self, template):
        # objeto odoo de correo
        mail = self.env['mail.mail']
        mail_data={
            'subject': 'Reporte de Crédito General',
            'body_html': template['body_html'],
            'email_to': 'francisco-castillo-moo@hotmail.com',
            'email_from': template['email_from'],
        }
        mail_out=mail.create(mail_data)
        if mail_out:
            mail.send(mail_out)
            _logger.info("Reporte de Pos Enviado")